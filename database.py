import sqlite3


class database:
    def __init__(self, filename):
        self.connection = sqlite3.connect( filename )
        self.cursor = self.connection.cursor()
        print( self.check_users() )
        print( self.check_subscribes() )
        print( self.check_images() )

    def check_users(self):
        try:
            self.cursor.execute('SELECT * FROM users')
            return 'Table USERS already exists'
        except:
            self.cursor.execute('''CREATE TABLE users (user_id varchar(25) primary key,    
                                user_name varchar(25), state varchar(2),
                                last_message varchar(255) )''')
            return 'Creating table USERS'

    def check_images(self):
        try:
            self.cursor.execute('SELECT * FROM images')
            return 'Table IMAGES already exists'
        except:
            self.cursor.execute('''CREATE TABLE images (weather_name varchar(50) primary key,    
                                image varchar(25) )''')
            return 'Creating table IMAGES'
    
    def check_subscribes(self):
        try:
            self.cursor.execute('SELECT * FROM subscribes')
            return 'Table SUBSCRIBES already exists'
        except:
            self.cursor.execute('''CREATE TABLE subscribes (id integer primary key AUTOINCREMENT,
                                time varchar(255), city varchar(20), user_id varchar(25)) ''')
            return 'Creating table SUBSCRIBES'

        
    def get_user(self, user_id=0, all_users=False):
        if all_users: #Если поступил запрос на получение данных о всех пользователях в таблице
            self.cursor.execute('SELECT * FROM users')
        else:
            self.cursor.execute('SELECT * FROM users WHERE user_id="{0}"'.format(user_id))
        ans = self.cursor.fetchall()
        users = {}
        for user in ans:
            users[user[0]] = list( user[1:] )
        return users


    def add_user(self, user_id, user_name, status=0, last_message=''):
        try:
            self.cursor.execute('INSERT INTO users VALUES ( "{0}", "{1}", "{2}", "{3}" )'.format(user_id, user_name, status, last_message))
            self.connection.commit()
            return True
        except:
            return False

    def modify_user(self, user_id, user_name, status, last_message):
        try:
            self.cursor.execute('UPDATE users SET user_name="{1}", status="{2}", last_message="{3}" WHERE user_id="{0}"'.format(user_id, user_name, status, last_message))
            self.connection.commit()
            return True
        except:
            return False
        
    def get_subscribes(self):
        self.cursor.execute('SELECT * FROM subscribes')
        ans = self.cursor.fetchall()
        subscribes = {}
        for line in ans:
            subscribes[line[0]] = {'time': line[1], 'city' : line[2], 'user_id' : line[3]}
        return subscribes

    def add_subscribe(self, time, city, user_id):
        try:
            self.cursor.execute( 'INSERT INTO subscribes VALUES (NULL, "{0}", "{1}", "{2}")'.format(time, city, user_id) )
            self.connection.commit()
            return True
        except:
            return False

    def modify_subscribe(self, time, subId):
        try:
            self.cursor.execute('UPDATE subscribes SET time="{0}" WHERE id={1}'.format(time, subId))
            self.connection.commit()
            return True
        except Exception as e:
            return False

    def delete_subscribe(self, subId):
        try:
            self.cursor.execute('DELETE FROM subscribes WHERE id="{0}"'.format(subId))
            self.connection.commit()
            return True
        except:
            return False

    def get_images(self):
        self.cursor.execute('SELECT * FROM images')
        ans = self.cursor.fetchall()
        images = {}
        for line in ans:
            images[line[0]] = line[1]
        return images

    def add_image(self, weather, image):
        try:
            self.cursor.execute( 'INSERT INTO images VALUES ("{0}", "{1}")'.format(weather, image) )
            self.connection.commit()
            return True
        except:
            return False

    
