import vk
from random import randint
from database import database
import weather
import time
from datetime import datetime, timedelta
import re
from fuzzywuzzy import process


class Bot:
    def __init__(self, token):
        self.token = token
        self.session = vk.Session(access_token = token)
        self.api = vk.API(self.session)
        self.db = database('vkbot.db')
        self.users = self.db.get_user(all_users=True)
        self.subscribes = self.db.get_subscribes()
        self.images = self.db.get_images()
        self.greeting = 'Привет, {0}\nЯ бот погоды. Я помогу тебе сэкономить время и не лезть в интернет за прогнозом.\n Спасибо, что пользуешься)'
        self.messageBox = [] # {'user_id' : '', 'message' : '', 'attachment' : ''}
        self.functions = { '0' : self.state_0, '1' : self.state_1, '11' : self.state_11, '12': self.state_12, '13': self.state_13, '14': self.state_14 }
        self.mainLoop()
    
    def mainLoop(self):
        while True:
            try:
                dialogs = self.api.messages.getDialogs(unanswered=1, v=5.92)
            except:
                self.session = vk.Session(access_token = self.token)
                self.api = vk.API(self.session)
            time.sleep(0.5)
            self.subManager()
            if dialogs['count'] != 0:
                user_id = str( dialogs['items'][0]['message']['user_id'] )
                message = dialogs['items'][0]['message']['body']
                user_info = self.api.users.get(user_ids=user_id, v=5.92)
                user_name = user_info[0]['first_name']
                self.checkUser(user_id, user_name)
                self.editMessage(message, user_id)
            for m in self.messageBox:
                self.sendMessage(m)
                self.messageBox.remove(m)
                

    def subManager(self):
        currentTime = datetime.now()
        currentTime = datetime(currentTime.year, currentTime.month, currentTime.day, currentTime.hour, currentTime.minute)
        self.subscribes = self.db.get_subscribes()
        self.subActuator()
        for sub in self.subscribes:
            t = datetime.strptime(self.subscribes[sub]['time'], '%Y-%m-%d %H:%M:%S')
            if currentTime == t:
                w = weather.get_weather(self.subscribes[sub]['city'], weather.key)
                if 'current_temp' in w:
                    ans = 'Сегодня в городе {0}: \n Температура: {1} \n Погода: {2}'.format(self.subscribes[sub]['city'],
                                                                                    w['current_temp'],
                                                                                    w['w_desc'])
                    self.messageBox.append( {'user_id' : self.subscribes[sub]['user_id'], 'value' : ans, 'att' : ''} )
                    t += timedelta(days = 1)
                    self.db.modify_subscribe(t, sub)

    def subActuator(self):
        currentTime = datetime.now()
        for sub in self.subscribes:
            t = datetime.strptime(self.subscribes[sub]['time'], '%Y-%m-%d %H:%M:%S')
            while currentTime > t:
                t += timedelta(days = 1)
                self.db.modify_subscribe(t, sub)
        
    def checkUser(self, user_id, user_name):
        if user_id not in self.users:
            self.users[user_id] = [user_name, '0', '']
            self.db.add_user(user_id, user_name)      

    def state_0(self, args):
        self.users[args['user_id']][1] = '1'
        greeting = self.greeting.format( args['username'] )
        self.messageBox.append( {'user_id' : args['user_id'], 'value' : greeting, 'att' : ''} )
        ans = 'Вы можете воспользоваться функциями:\n1. Получить погоду\n2. Получить прогноз\n3. Получать погоду в указанное время\n4. Отписаться от получения погоды в указанное время'
        self.messageBox.append( {'user_id' : args['user_id'], 'value' : ans, 'att' : ''} )

    def state_1(self, args):
        if args['message'].lower().strip() in ['1', 'получить погоду']:
            self.users[args['user_id']][1] = '11'
            self.messageBox.append( {'user_id' : args['user_id'], 'value' : 'Введите название города', 'att' : ''} )
        elif args['message'].lower().strip() in ['2', 'получить прогноз']:
            self.users[args['user_id']][1] = '12'
            self.messageBox.append( {'user_id' : args['user_id'], 'value' : 'Введите название города', 'att' : ''} )
        elif args['message'].lower().strip() in ['3', 'подписаться на прогноз']:
            self.users[args['user_id']][1] = '13'
            m = 'Пришлите время, в которое хотите получать прогноз погоды в вашем городе.\nФормат сообщения должен быть - "чч:мм Город"'
            self.messageBox.append( {'user_id' : args['user_id'], 'value' : m, 'att' : ''} )
        elif args['message'].lower().strip() in ['4', 'отписаться от получения погоды']:
            self.users[args['user_id']][1] = '14'
            m = 'Выберите номер подписки, которую хотите отменить (Либо напишите "Отмена"):\n\n'
            for sub in self.subscribes:
                if self.subscribes[sub]['user_id'] == args['user_id']:
                    t = self.subscribes[sub]['time'][-8:]
                    m += '{0}. В "{1}" в указанное время: {2}\n'.format(sub, self.subscribes[sub]['city'], t)
            self.messageBox.append( {'user_id' : args['user_id'], 'value' : m, 'att' : ''} )
        else:
            ans = 'Вы можете воспользоваться функциями:\n1. Получить погоду\n2. Получить прогноз\n3. Получать погоду в указанное время\n4. Отписаться от получения погоды в указанное время'
            self.messageBox.append( {'user_id' : args['user_id'], 'value' : 'К сожалению, я вас не понимаю(', 'att' : ''} )
            self.messageBox.append( {'user_id' : args['user_id'], 'value' : ans, 'att' : ''} )


    def state_14(self, args):
        self.users[args['user_id']][1] = '1'
        if args['message'].lower().strip() == 'отмена':
            self.messageBox.append( {'user_id' : args['user_id'], 'value' : 'Вы вернулись в главное меню', 'att' : ''} )
            ans = 'Вы можете воспользоваться функциями:\n1. Получить погоду\n2. Получить прогноз\n3. Получать погоду в указанное время\n4. Отписаться от получения погоды в указанное время'
            self.messageBox.append( {'user_id' : args['user_id'], 'value' : ans, 'att' : ''} )
            return
        if args['message'].isdigit():
            if self.subscribes[int(args['message'])]['user_id'] == args['user_id']:
                if self.db.delete_subscribe(args['message']):
                    self.messageBox.append( {'user_id' : args['user_id'], 'value' : 'Подписка отключена', 'att' : ''} )
                    ans = 'Вы можете воспользоваться функциями:\n1. Получить погоду\n2. Получить прогноз\n3. Получать погоду в указанное время\n4. Отписаться от получения погоды в указанное время'
                    self.messageBox.append( {'user_id' : args['user_id'], 'value' : ans, 'att' : ''} )
                else:
                    self.messageBox.append( {'user_id' : args['user_id'], 'value' : 'Не удалось отключить подписку', 'att' : ''} )
                    ans = 'Вы можете воспользоваться функциями:\n1. Получить погоду\n2. Получить прогноз\n3. Получать погоду в указанное время\n4. Отписаться от получения погоды в указанное время'
                    self.messageBox.append( {'user_id' : args['user_id'], 'value' : ans, 'att' : ''} )
            else:
                self.messageBox.append( {'user_id' : args['user_id'], 'value' : 'Невозможно отключить эту подписку', 'att' : ''} )
                ans = 'Вы можете воспользоваться функциями:\n1. Получить погоду\n2. Получить прогноз\n3. Получать погоду в указанное время\n4. Отписаться от получения погоды в указанное время'
                self.messageBox.append( {'user_id' : args['user_id'], 'value' : ans, 'att' : ''} )
        else:
            ans = 'Нерпавильно введен номер подписки'
            self.messageBox.append( {'user_id' : args['user_id'], 'value' : ans, 'att' : ''} )
            ans = 'Вы можете воспользоваться функциями:\n1. Получить погоду\n2. Получить прогноз\n3. Получать погоду в указанное время\n4. Отписаться от получения погоды в указанное время'
            self.messageBox.append( {'user_id' : args['user_id'], 'value' : ans, 'att' : ''} )
    
    def state_12(self, args):
        w = weather.get_forecast(args['message'], weather.key)
        if type(w) == type(list()):
            w = w[::4]
            template = '{3} в городе {0}: \n Температура: {1} \n Погода: {2}\n\n'
            ans = ''
            for point_w in w:
                ans += template.format(args['message'],
                                        point_w['current_temp'],
                                        point_w['w_desc'],
                                        point_w['timestamp'])
            self.users[args['user_id']][1] = '1'
            self.messageBox.append( {'user_id' : args['user_id'], 'value' : ans, 'att' : ''} )
            
        else:
            self.users[args['user_id']][1] = '1'
            self.messageBox.append( {'user_id' : args['user_id'], 'value' : w, 'att' : ''} )
        ans = 'Вы можете воспользоваться функциями:\n1. Получить погоду\n2. Получить прогноз\n3. Получать погоду в указанное время\n4. Отписаться от получения погоды в указанное время'
        self.messageBox.append( {'user_id' : args['user_id'], 'value' : ans, 'att' : ''} )
            
            
    def state_13(self, args):
        message_regexp = re.compile('[0-2][0-9]:[0-5][0-9] .*')
        time_city = message_regexp.findall(args['message'])
        if len(time_city) != 1:
            ans = 'Неправильный ввод, повторите попытку'
            self.messageBox.append( {'user_id' : args['user_id'], 'value' : ans, 'att' : ''} )
            return
        time_city = time_city[0].split()
        time = time_city[0].split(':')
        print(time)
        h = int(time[0])
        m = int(time[1])
        city = time_city[1]
        current_time = datetime.now()
        if 'current_temp' not in weather.get_weather(city, weather.key):
            self.messageBox.append( {'user_id' : args['user_id'],
                                     'value' : 'Неправильно введен город, повторите попытку.\nФормат сообщения должен быть - "чч:мм Город"',
                                     'att' : ''} )
        elif h >= 24 or h < 0 or m >= 60 or m < 0:
            self.messageBox.append( {'user_id' : args['user_id'],
                                     'value' : 'Неправильно введено время, повторите попытку.\nФормат сообщения должен быть - "чч:мм Город"',
                                     'att' : ''} )
        else:
            if current_time.hour > h or (current_time.hour == h and current_time.minute >= m):
                t = datetime(current_time.year, current_time.month, current_time.day, h, m)
                t += timedelta(days=1)
                self.db.add_subscribe(t, city, args['user_id'])
                self.messageBox.append( {'user_id' : args['user_id'], 'value' : 'Подписка совершена', 'att' : ''} )
                ans = 'Вы можете воспользоваться функциями:\n1. Получить погоду\n2. Получить прогноз\n3. Получать погоду в указанное время\n4. Отписаться от получения погоды в указанное время'
                self.messageBox.append( {'user_id' : args['user_id'], 'value' : ans, 'att' : ''} )
                self.users[args['user_id']][1] = '1'
            else:
                t = datetime(current_time.year, current_time.month, current_time.day, h, m)
                self.db.add_subscribe(t, city, args['user_id'])
                self.messageBox.append( {'user_id' : args['user_id'], 'value' : 'Подписка совершена', 'att' : ''} )
                ans = 'Вы можете воспользоваться функциями:\n1. Получить погоду\n2. Получить прогноз\n3. Получать погоду в указанное время\n4. Отписаться от получения погоды в указанное время'
                self.messageBox.append( {'user_id' : args['user_id'], 'value' : ans, 'att' : ''} )
                self.users[args['user_id']][1] = '1'

            self.subscribes = self.db.get_subscribes()

        
    def state_11(self, args):
        w = weather.get_weather(args['message'], weather.key)
        weathers = list(self.images.keys())
        if 'current_temp' in w:
            ans = 'Сегодня в городе {0}: \n Температура: {1} \n Погода: {2}'.format(args['message'],
                                                                                    w['current_temp'],
                                                                                    w['w_desc'])
            self.users[args['user_id']][1] = '1'
            img_key = process.extractOne(w['w_desc'], weathers)[0]
            self.messageBox.append( {'user_id' : args['user_id'], 'value' : ans, 'att' : self.images[img_key]} ) 
        else:
            self.messageBox.append( {'user_id' : args['user_id'], 'value' : w, 'att' : ''} )
            self.users[args['user_id']][1] = '1'
        ans = 'Вы можете воспользоваться функциями:\n1. Получить погоду\n2. Получить прогноз\n3. Получать погоду в указанное время\n4. Отписаться от получения погоды в указанное время'
        self.messageBox.append( {'user_id' : args['user_id'], 'value' : ans, 'att' : ''} )
            
    
    def editMessage(self, message, user_id):
        user_state = self.users[user_id][1]
        args = {}
        args['username'] = self.users[user_id][0]
        args['message'] = message
        args['user_id'] = user_id
        if user_state in self.functions:
            self.functions[user_state](args)
            
    
    def sendMessage(self, message):
        try:
            self.api.messages.send( user_id = message['user_id'], random_id = randint(0, 100000),
                       message = message['value'], attachment = message['att'] , v=5.92)
            return 'Сообщение отправлено'
        except:
            return 'Не удалось отправить сообщение пользователю {0}'.format(user_id)



b = Bot(token='11ca979a9e7bb575709d9320e75eda150cf20bb93e562a55c63fec53856a2be240b70e1d112b511463222')






